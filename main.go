package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	odbc "github.com/OpenInformix/IfxGo"
	"log"
	"time"
)

var (
	ifxsrv  = flag.String("ifxsrv", "ol_pyp_devhd_shm", "ifx server name")
	ifxdb   = flag.String("ifxdb", "uhd10", "ifx database name")
	ifxuser = flag.String("ifxuser", "uhd10", "ifx user name")
	ifxpass = flag.String("ifxpass", "5hD$P3r5", "ifx password")
)

func ifxConnect() (db *sql.DB, stmtCount int, err error) {
	//conn := fmt.Sprintf("driver=/Applications/dsdriver/lib/libdb2.dylib;server=%s;database=%s;UID=%s;PWD=%s", *ifxsrv, *ifxdb, *ifxuser, *ifxpass)
	//conn := fmt.Sprintf("Driver=/Applications/dsdriver/lib/libdb2.dylib;Server=%s;DataBase=%s;HostName=35.188.247.205;Uid=%s;Pwd=%s;Service=1526;Protocol=ONSOCTCP;", *ifxsrv, *ifxdb, *ifxuser, *ifxpass)
	//conn := fmt.Sprintf("Driver=/Applications/dsdriver/lib/libdb2.dylib;Host=35.188.247.205;Server=%s;Database=%s;Uid=%s;Password=%s;", *ifxsrv, *ifxdb, *ifxuser, *ifxpass)
	//conn := fmt.Sprintf("Dsn=urbano;", *ifxsrv, *ifxdb, *ifxuser, *ifxpass)
	conn := fmt.Sprintf("Driver=/home/informix/lib/cli/libifcli.so;Server=%s;Database=%s;Host=10.142.0.9;Uid=%s;Pwd=%s;Service=1526;Protocol=onsoctcp;", *ifxsrv, *ifxdb, *ifxuser, *ifxpass)
	db, err = sql.Open("odbc", conn)
	if err != nil {
		return nil, 0, err
	}
	log.Print("Successfully called open()")
	stats := db.Driver().(*odbc.Driver).Stats

	//Prepare a "context" to execute queries in, this will allow us to use timeouts
	var bgCtx = context.Background()
	var ctx2SecondTimeout, cancelFunc2SecondTimeout = context.WithTimeout(bgCtx, time.Second*2)
	defer cancelFunc2SecondTimeout()

	//Ping database connection with 2 second timeout
	if err := db.PingContext(ctx2SecondTimeout); err != nil {
		log.Printf("Can't ping database server in order to use storage, %s", err.Error())
		return nil, 0, err
	}

	return db, stats.StmtCount, nil
}

/*func TestIFXTime(t *testing.T) {
	db, sc, err := ifxConnect()
	if err != nil {
		t.Fatal(err)
	}
	defer closeDB(t, db, sc, sc)

	db.Exec("drop table temp")
	exec(t, db, "create table temp(id serial primary key, time datetime year to second)")
	now := time.Now()
	// SQL_TIME_STRUCT only supports hours, minutes and seconds
	now = time.Date(1, time.January, 1, now.Hour(), now.Minute(), now.Second(), 0, time.Local)
	_, err = db.Exec("insert into temp (time) values(?)", now)
	if err != nil {
		t.Fatal(err)
	}

	var ret time.Time
	if err := db.QueryRow("select time from temp where id = ?", 1).Scan(&ret); err != nil {
		t.Fatal(err)
	}
	if ret != now {
		t.Fatalf("unexpected return value: want=%v, is=%v", now, ret)
	}

	exec(t, db, "drop table temp")
}*/

func main() {
	db, sc, err := ifxConnect()
	if err != nil {
		log.Printf("Unable to open db, %s", err.Error())
		return
	}

	log.Print("Successfully called open()", sc)

	db.Close()
}
