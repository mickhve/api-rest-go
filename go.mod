module informix

go 1.14

require (
	github.com/OpenInformix/IfxGo v0.0.0-20180503180937-851536d13269
	github.com/alexbrainman/odbc v0.0.0-20200426075526-f0492dfa1575 // indirect
	golang.org/x/sys v0.0.0-20200622214017-ed371f2e16b4 // indirect
)
